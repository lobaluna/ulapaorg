+++
title = "Gobernanza"
date = 2022-07-06T20:03:56-05:00
draft = false
+++

## 25 puntos: Instrumento para la gobernanza global de la Unión Latinoamericana de Pacientes

1.  ULAPA es una unión de organizaciones paraguas de América Latina y el Caribe, sean estas federaciones, alianzas, fundaciones o asociaciones, todas con personería jurídica.
2.  Las organizaciones paraguas interesadas en formar parte de ULAPA deberán enviar una petición por correo electrónico a ulapaorg@gmail.com, solicitando una reunión en la que conoceremos el proyecto que desean integrar a nuestro esfuerzo. En todo caso la cita no deberá hacerse esperar más de diez días hábiles a partir de la recepción de la petición. Luego de la entrevista, ULAPA responderá si entendemos elegible su proyecto en un plazo no mayor a una semana. Este compromiso de membresía deberá reconfirmarse anualmente.
3.  Puede haber más de una organización miembro por país, pero cada país tiene un solo voto.
4.  Cada organización de país miembro designará formalmente 2 líderes representantes de su organización/país ante la ULAPA.
5.  La adhesión de los miembros a cualquier medida que tome ULAPA se sellará con la firma electrónica de los dos líderes designados por cada organización.
6.  En ULAPA no se pagarán membresías. Los miembros aportarán lo que deseen aportar, se llevará registro de los gastos y las donaciones que espontáneamente surjan en cada país miembro, con el acuerdo de los 2 líderes representantes de cada organización paraguas de pacientes.
7.  ULAPA tendrá un vocero único permanente más allá de que allí donde estén los miembros de ULAPA podrán alzar su voz. ULAPA no tiene comisión directiva. El modelo ULAPA no parte de la subordinación y está determinado por la creatividad, valentía e inteligencia independiente de sus miembros, quienes deberán determinar bajo su responsabilidad y criterio si están actuando en el espíritu de la ley de los intereses de los pacientes de la región. El vocero permanente solo podrá removerse por cuestiones específicas graves que así lo ameritaran y que surgieran de una Asamblea Extraordinaria.
8.  Los miembros de ULAPA se verán obligados a reclamar informes de gestión anuales a sus gobiernos particulares en las áreas de la salud y la discapacidad; estos informes que se presentarán en asamblea general a ULAPA.
9.  Los compromisos de trabajo deberán respetar la fecha de entrega previamente pautada.
10. Las reuniones necesitarán de la presencia de al menos uno de los líderes miembros por país. El voto no podrá delegarse.
11. ULAPA tendrá una Comisión Permanente para participar de eventos internacionales que pudieran presentarse sin previo aviso.
12. ULAPA presentará un informe de gestión anual elaborado en Asamblea General a todos sus asociados de cada organización miembro y a los gobiernos de la región.
13. ULAPA premiará anualmente las buenas prácticas y proyectos de líderes, organizaciones, médicos, instituciones, políticos e investigadores de todo el mundo. El premio ULAPA se entregará el 28 de febrero a la comunidad internacional.
14. Los días viernes de cada semana, los jefes de prensa y comunicación de las organizaciones miembro enviarán los contenidos que el encargado de prensa de ULAPA subirá a las redes de todo lo que las organizaciones hayan hecho en los últimos siete días.
15. Las asambleas generales y reuniones de trabajo no temáticas tendrán lugar los terceros sábados de cada mes, de 12 a 14 horas (horario de Argentina). En caso de necesidad consensuada se podrá prolongar la reunión por media hora más.
16. Las asambleas extraordinarias o reuniones temáticas específicas y puntuales se pautarán libremente entre los participantes de dicha reunión.
17. Las asambleas deberán ejecutar una minuta colectiva y, a su vez, una vez finalizada esta, se elaborará un acta aprobada por los miembros presentes que se compartirá con los miembros por correo electrónico.
18. Atendiendo a la diversidad lingüística de la región y a las diferentes modalidades y usos y costumbres de cada uno de nuestros pueblos, proponemos ser tolerantes respecto de los modos específicos que utilizamos los latinoamericanos para comunicarnos, en un clima de respeto y cordialidad.
19. En caso de empate en las votaciones, tendrán derecho a doble voto los miembros fundacionales.
20. Cuando se le encargue una tarea a un miembro o un miembro se comprometa a aportar una determinada cosa, los demás tomarán por bueno lo que el miembro haya aportado.
21. La deserción de un miembro por el motivo que fuera deberá anunciarse con 30 días de antelación, para poder organizarnos al respecto y para poder establecer una estrategia de reemplazo conveniente a ULAPA.
22. El grupo de “La Vecindad ULAPA” es el grupo dirigido a pacientes y familiares de pacientes de entre 13 y 19 años de América Latina y el Caribe. Estos adolescentes serán promocionados como embajadores ULAPA.
23. El grupo de “Jóvenes de ULAPA” es el grupo de pacientes y familiares de pacientes de entre 20 y 35 años de América Latina y el Caribe. ULAPA les dará capacitación específica y talleres culturales tendientes a fortalecer y empoderar a sus futuros líderes.
24. Modelo social de la discapacidad. ULAPA propende a la unión militante de los líderes y las luchas del campo de la discapacidad y de la salud.
25. En caso de conflicto o de ausencias reiteradas a las reuniones de trabajo, ULAPA se verá en la necesidad de expulsar a la organización miembro responsable de la falta en cuestión luego de llamar a Asamblea Extraordinaria.


### Nuestros objetivos permanentes

El objetivo de acceso a la salud que mejor se oferte en plaza y el objetivo del desarrollo médico científico de nuestros pueblos nunca desaparecerán de nuestra mira.

ULAPA es sinónimo de lucha por la equidad y por la justicia sanitaria para todas las personas que habiten nuestro hermoso continente.

ULAPA es pasión, entrega y determinación a favor de las mejores causas.

