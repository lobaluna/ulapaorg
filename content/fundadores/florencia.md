+++
title = "FLORENCIA Braga Menéndez"
date = 2022-07-01T13:47:39-05:00
type = "fundadores"

nombre = "FLORENCIA"
foto = "images/fundadores/ARG-Florencia.jpg"
correoe = "florenciabraga@alianzapacientes.org"
organizacion = "ALAPA"
pais = "ARGENTINA"

+++

Actualmente es copresidente de ALAPA, la Alianza Argentina de Pacientes, y miembro fundador de ULAPA, la Unión Latinoamericana de Pacientes con enfermedades poco frecuentes, raras o de baja prevalencia.

Ella es especialista en gestión cultural internacional. Además, es una exbecaria de la Fundación Carolina de España y del Departamento de Estado de los Estados Unidos de América IVLP. Fue directora de Museos del Gobierno de la Ciudad de Buenos Aires (GCBA) y presidenta de la Comisión de Discapacidad del GCBA.

A partir de que su hijo fuera diagnosticado en el año 2010 con la enfermedad de Stargardt, una enfermedad oftalmológica degenerativa poco frecuente (EPOF/ER), cierra su espacio de exhibición, deja su trabajo como curadora de arte contemporáneo y comienza a hacer _advocacy_ internacional para conseguir atención y tratamientos para estas enfermedades raras.

Ha publicado su trabajo y representado al país en congresos y eventos internacionales.

Junto a la psicóloga Carolina Oliveto crea y preside la Alianza Argentina de Pacientes (ALAPA), donde trabajan para pacientes de más de 200 diferentes patologías poco frecuentes.

ALAPA es miembro de IAPO, UDNI, ALIBER, Rare Diseases International, EURORDIS, Alianza Latina, el Observatorio Latinoamericano de Bioética y Derechos de los Pacientes de la UNDAV y la Unión Latinoamericana de Pacientes con Enfermedades Poco Frecuentes (ULAPA), de la que es cofundadora junto a otros líderes latinoamericanos de pacientes.

Coordina, junto al Dr. Marcelo Pereyra, el Centro de Excelencia en Medicina Traslacional (CEMET) del Hospital El Cruce.

Escribe sobre arte, cultura, derechos humanos y política sanitaria. Habla y escribe en español, inglés y francés.

