+++
title = "CAROLINA Oliveto"
slug = "carolina"
date = 2022-07-04T19:11:26-05:00
type = "fundadores"

nombre = "CAROLINA"
foto = "images/fundadores/ARG-Carolina.jpg"
correoe = "caro@alapa.org"
organizacion = "ALAPA"
pais = "ARGENTINA"
+++

Carolina Oliveto es una psicóloga que…

Ella es cofundadora y directora de Programas en ALAPA, la Alianza Latinoamericana de Pacientes. También es directora de l Centro de Salud Mental de ALAPA.

Carolina padece el síndrome de Stargardt.

Ella habla y escribe en español e italiano.
