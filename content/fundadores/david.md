+++
title = "DAVID Peña Castillo"
date = 2022-07-02T17:00:24-05:00
type = "fundadores"

nombre = "DAVID"
correoE = "david@femexer.org"
foto = "images/fundadores/MEX-David.jpg"
organizacion = "FEMEXER"
pais = "MÉXICO"
+++

Licenciado en Administración Pública y Ciencias Políticas por la Universidad Nacional Autónoma de México (UNAM). Diplomado en Ciencias Políticas e Historia de la Administración Pública en México, por el Colegio de México.

Padre de dos hijos con la enfermedad de Gaucher.

Experto en cabildeo para la agenda pública en salud de México y del entorno global (OMS y OPS). Líder de 90 asociaciones nacionales con enfermedades raras, las cuales representan a 4.5 millones de habitantes en México con estos padecimientos.

Actualmente es presidente del Proyecto Pide un Deseo México (PPuDM) y de la Federación Mexicana de Enfermedades Raras (FEMEXER).

Responsable y coordinador de los primeros y más importantes foros que en México hablaron de las enfermedades raras, desde 1996 a la fecha. Participante en más de 400 eventos alrededor del mundo en atención a las enfermedades raras.

Miembro fundador de Proyecto Pide un Deseo México (PPuDM, 1996), de la Federación Mexicana de Enfermedades Raras (FEMEXER, 2011) y de AcceSalud, el programa de información, orientación y apoyo psicológico de FEMEXER, en 2014.

Miembro de Rare Diseases International (RDI), Eurordis, NORD, Rare Genes, ICORD, IGA, IPA, FIN, FUNEDERE y ULAPA.

