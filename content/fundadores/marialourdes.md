+++
title = "MARÍA LOURDES Rodríguez Heudebert de Berckemeyer"
date = 2022-07-04T11:43:36-05:00
type = "fundadores"

nombre = "MARÍA LOURDES"
foto = "images/fundadores/PER-MariaLourdes.jpg"
correoE = "mitsuko54@yahoo.com"
organizacion = "FEPER"
pais = "PERÚ"
+++

La Sra. María Lourdes Rodríguez Heudebert de Berckemeyer estudió arqueología, trabajó en el diario El Comercio y en Media Networks Telefónica del Perú.

Es miembro fundador y presidenta de la Federación Peruana de Enfermedades Raras (FEPER). Es fundadora y presidenta de la Asociación Hecho con Amor de Esclerosis Múltiple. Es fundadora de la Junta de Usuarios de Salud (JUS).

Pertenece a la red Laten-Msif, a la Alianza Nacional del Cáncer, a los aliados de los grupos de discapacidad, entre otras múltiples alianzas u organizaciones nacionales e internacionales.

Fue condecorada mediante la Resolución de Alcaldía 096, fue premio y condecoración Lima 2020 como destacada Mujer Gestora Social por su trayectoria de 15 años.

Escribió el libro Mujeres del Bicentenario.

Ha participado en diversas mociones de saludo en varios años en el Congreso de la República por la búsqueda permanente en la obtención de mejores tratamientos, derechos fundamentales y ayuda humanitaria para las personas con enfermedades raras.

Debido a los múltiples diagnósticos que fue recibiendo de forma progresiva, ella inició una labor filantrópica para ayudar a las personas en situación socioeconómica adversa con o sin seguro, especialmente de las áreas rurales, para las personas que tienen o presumen tener enfermedades raras y esclerosis múltiple.
	
María Lourdes es promotora de la Ley 29698 que declara de interés nacional y atención preferente a las personas con enfermedades raras. Juntamente con otras asociaciones de pacientes, promovieron y participaron activamente en el desarrollo del Reglamento Nacional de Enfermedades Raras, la lista de enfermedades con pacientes identificados y el desarrollo de Plan Nacional 2021-2024. Su participación ha sido permanente en la continuidad de la hoja de ruta sobre el abordaje de las enfermedades raras y sus familias en todos los ámbitos de su vida: salud, educación, promoción social, investigación, ayuda humanitaria, defensa de sus derechos, entre otros.
