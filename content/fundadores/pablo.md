+++
title = "PABLO J. Correa Garín"
date = 2022-07-04T12:02:46-05:00
type = "fundadores"

nombre = "PABLO"
foto = "images/fundadores/URY-Pablo.jpg"
correoE = "pabloc2099@gmail.com"
organizacion = "FAME Uruguay"
pais = "URUGUAY"
+++

Soy padre de Santiago, un joven de 19 años con atrofia muscular espinal tipo 1 (Werdnig-Hoffmann). Su diagnóstico a los 6 meses de vida como enfermedad rara o poco frecuente me ha hecho involucrarme en mejorar la calidad de vida y el acceso a terapias, medicación, insumos, etc.

De profesión, soy conductor profesional (transporte público).

Soy cofundador y presidente de las Familias con Atrofia Espinal Uruguay (FAME URUGUAY), cofundador de la Alianza Latinoamericana Atrofia Muscular Espinal (ALAME), cofundador de la Unión Latinoamericana de Pacientes (ULAPA), secretario de la Federación Uruguaya de la Discapacidad y EERR (FUDI), integrante de la Alianza de Pacientes Uruguay, integrante de la Comisión Honoraria de la Discapacidad Uruguay (CNHD), integrante del Comité Consultivo de Discapacidad y Cuidados en el Ministerio de Desarollo Social Uruguay.
 
Tengo estudios escolares de secundaria y un diplomado en alfabetización en salud.
